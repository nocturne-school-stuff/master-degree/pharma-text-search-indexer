from json import dumps
import pandas as pd

from ...index.index import getOrCreateIndex

def parseTSV(filePath, properties, cachePath, pattern):
    with open(cachePath, "a", encoding="utf-8") as cacheFile: 
        w = getOrCreateIndex(pattern).writer()
        df = pd.read_csv(filePath, sep="\t", encoding="utf-8")
        
        for _, row in df.iterrows():
            rowObject = {}
            for p, t in properties.items():
                rowObject[p] = t(row)
            w.add_document(**rowObject)
            print(dumps(rowObject), file=cacheFile)

    w.commit()