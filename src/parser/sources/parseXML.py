from json import dumps
from xml.etree import cElementTree as ET

from ...index.index import getOrCreateIndex

def parseXML(filePath, properties, searchedTag, cachePath, pattern, minimalPropsCount = 0) : 
    with open(cachePath, "a", encoding="utf-8") as cacheFile: 
        with open(filePath, 'rb') as xmlFile:    
            found = True
            
            w = getOrCreateIndex(pattern).writer()
            for evt, element in ET.iterparse(xmlFile, events=('start', 'end')):
                if element.tag.endswith(searchedTag) and evt == "start":
                    found = True
                    rowObject = {}         

                if found and evt == "start":
                    p = element.tag.split("}")[-1]
                    if p in properties:
                        rowObject[p] = properties[p](element)

                if element.tag.endswith(searchedTag) and evt == "end":
                    found = False
                    if minimalPropsCount <= len(rowObject.keys()):
                        reformatedAttributes = {k.replace("-", "_"): v for k, v in rowObject.items()}
                        w.add_document(**reformatedAttributes)
                        print(dumps(reformatedAttributes), file=cacheFile)

                    rowObject = {}

                element.clear()
        w.commit()
    