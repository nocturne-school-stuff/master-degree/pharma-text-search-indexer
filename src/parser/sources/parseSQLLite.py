from json import dumps
import sqlite3

from ...index.index import getOrCreateIndex

def parseSQLLite (dbFilePath, properties, cachePath, pattern, query):
    with open(cachePath, "a") as cacheFile: 
        w = getOrCreateIndex(pattern).writer()

        conn = sqlite3.connect(dbFilePath)
        cursor = conn.cursor()
        cursor.execute(query)

        for row in cursor.fetchall():
            rowObject = {}
            for p, t in properties.items():
                rowObject[p] = t(row)
            w.add_document(**rowObject)
            print(dumps(rowObject), file=cacheFile)

        conn.close()
        w.commit()