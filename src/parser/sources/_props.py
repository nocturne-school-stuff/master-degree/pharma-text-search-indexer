from pandas import notna

ATCProps = {
    "cid": lambda row: row[1], 
    "source": lambda row : row[2],
    "atc-code": lambda row : row[3]
}

Keg = {
    "source": lambda row: row[1],
    "chemical": lambda row: " ".join(row[2:])
}

DrugBank = {
    "name": lambda e : e.text,
    "indication": lambda e : e.text,
    "toxicity": lambda e : e.text,
    "atc-code": lambda e : e.attrib["code"]
}

OMIM_onto = {
    "classId": lambda row : row["Class ID"].split("/")[-1],
    "cui": lambda row : row["CUI"] if notna(row["CUI"]) else None
}

OMIMtxt = {
    "NO": lambda content : content,
    "CS": lambda content : content,
    "TI": lambda content : content
}

HPOOBO = {
    "id": lambda node : node["id"],
    "name": lambda node : node["name"],
    "is_a": lambda node : node["is_a"],
    "alt_id": lambda node : node["alt_id"]
}

HPOAnnotation = {
    "disease_label": lambda row : row[2],
    "sign_id": lambda row : row[4]
}

Meddra = {
    "meddra_id" : lambda row : f"{row[2]}",
    "meddra_cui" : lambda row : row[0],
    "meddra_concept_type" : lambda row : row[1],
    "meddra_concept_name" : lambda row : row[3]
}

MeddraAllIndication = {
    "stich_compound_id" : lambda row : row[0],
    "meddra_cui" : lambda row : row[1],
    "concept_name" : lambda row : row[3],
    "method_of_detection" : lambda row : row[2],
    "meddra_concept_type" : lambda row : row[4] if notna(row[4]) else None,
    "meddra_concept_name" : lambda row : row[5] if notna(row[5]) else None
}

MeddraAllSe = {
    "meddra_cui": lambda row : row[0],
    "cui": lambda row : row[1],
    "stich_compound_id1": lambda row : row[2],
    "stich_compound_id2": lambda row : row[2] if notna(row[2]) else None,
    "meddra_concept_type": lambda row : row[3] if notna(row[3]) else None,
    "side_effect": lambda row : row[5]
}

MeddraFreq = {
    "cui" : lambda row : row[2],
    "stich_compound_id2" : lambda row : row[1] if notna(row[1]) else None,
    "stich_compound_id1" : lambda row : row[0],
    "frequency_description" : lambda row : row[4],
    "freq_lower_bound" : lambda row : row[5],
    "freq_upper_bound" : lambda row : row[6],
    "placebo" : lambda row : row[3] if notna(row[3]) else None,
    "side_effect" : lambda row : row[9],
    "meddra_concept_type" : lambda row : row[7] if notna(row[7]) else None,
    "meddra_concept_id" : lambda row : row[8] if notna(row[8]) else None
}
