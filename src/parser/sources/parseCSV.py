from json import dumps
import pandas as pd

from ...index.index import getOrCreateIndex

def parseCSV(filePath, properties, cachePath, pattern) :
    with open(cachePath, "a") as cacheFile: 
        df = pd.read_csv(filePath)
        
        w = getOrCreateIndex(pattern).writer()
        for _, row in df.iterrows():
            rowObject = {}
            for p, t in properties.items():
                rowObject[p] = t(row) 
            w.add_document(**rowObject)
            print(dumps(rowObject), file=cacheFile)

    w.commit()