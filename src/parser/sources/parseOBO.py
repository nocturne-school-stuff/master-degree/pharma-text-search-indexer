from json import dumps
from ...index.index import getOrCreateIndex
from obonet import read_obo

def parseOBO (OBOFilePath, properties, cachePath, pattern) :
    with open(cachePath, "a") as cacheFile: 
        graph = read_obo(OBOFilePath)

        idx = getOrCreateIndex(pattern)
        w = idx.writer()
        for id , node in graph.nodes(data=True):
            rowObject = {}
            node["id"] = id
            invalidNode = False
            for p, t in properties.items():
                try:
                    rowObject[p] = t(node)
                except:
                    invalidNode = True
                if invalidNode: break
            if not invalidNode:
                w.add_document(**rowObject)
                print(dumps(rowObject), file=cacheFile)    

    w.commit()