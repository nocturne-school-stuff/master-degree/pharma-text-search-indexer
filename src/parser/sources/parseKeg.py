from json import dumps
from ...index.index import getOrCreateIndex

def parseKeg(filePath, properties, cachePath, pattern, linePrefix = "E") : 
    with open(cachePath, "a") as cacheFile: 
        w = getOrCreateIndex(pattern).writer()
        with open(filePath, "r") as kegFile:
            result = []        
            for line in kegFile.readlines():
                if not line.startswith(linePrefix): continue

                row = list(filter(lambda x: x != '', line.replace("\n", "").split(" ")))
                rowObject = {}
                for p, t in properties.items():
                    rowObject[p] = t(row)
                w.add_document(**rowObject)
                print(dumps(rowObject), file=cacheFile)
        w.commit()