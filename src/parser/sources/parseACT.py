from json import dumps
from ...index.index import getOrCreateIndex

def parseACT(filePath, properties, cachePath, pattern):
    with open(cachePath, "a") as cacheFile: 
        w = getOrCreateIndex(pattern).writer()
        with open(filePath, "r") as inputFile:
            try:
                i = 1
                inputFile.readline()
                while True:
                    line = inputFile.readline()
                    if line.startswith("#"): continue
                    row = line.replace("\n", "").replace("\r", "").split("\t")
                    rowObject = {}
                    for p, t in properties.items():
                        rowObject[p] = t(row)
                    reformatedAttributes = {k.replace("-", "_"): v for k, v in rowObject.items()}
                    w.add_document(**reformatedAttributes)
                    print(dumps(reformatedAttributes), file=cacheFile)
                    i = i + 1
            except:
                pass
        w.commit()