from json import dumps
from ...index.index import getOrCreateIndex

def parseTXT(filePath, properties, cachePath, pattern):
    with open(cachePath, "a") as cacheFile: 
        with open(filePath, "r") as inputFile:
            w = getOrCreateIndex(pattern).writer()

            content = inputFile.readlines()
            propertiesField = list(map(lambda p : f"*FIELD* {p}", properties.keys()))
            for i in range(len(content)):
                if "*THEEND*" in content[i]:
                    break
                if "*RECORD*" in content[i]:
                    i = i + 1
                    rowObject = {}
                    while(not ("*RECORD*" in content[i + 1] or "*THEEND*" in content[i + 1])):
                        if any([pf in content[i] for pf in propertiesField]) :
                            p = content[i].split(" ")[1].replace("\n", "").replace("\r", "")
                            i = i + 1
                            j = i
                            while(not (content[i + 1].startswith("*FIELD*") or content[i + 1].startswith("*RECORD*") or content[i + 1].startswith("*THEEND*"))):
                                i = i + 1
                            rowObject[p] = properties[p]("".join(content[j:i+1])) 
                        i = i + 1
                    w.add_document(**rowObject)
                    print(dumps(rowObject), file=cacheFile)
                else:
                    i = i + 1
        w.commit()
                    