from time import time
from ..index.patterns import drugBankPattern, HPOOBOPattern, OMIMtxtPattern, OMIM_ontoPattern, HPOAnnotationPattern, meddraPattern, MeddraAllIndicationPattern, MeddraAllSePattern, MeddraFreqPattern, KegPattern, ATCPattern
from ..index.index import indexExist

from .sources._props import ATCProps, DrugBank, OMIM_onto, OMIMtxt, HPOAnnotation, HPOOBO, Meddra, MeddraAllIndication, MeddraAllSe, MeddraFreq, Keg

from .sources.parseACT import parseACT
from .sources.parseCSV import parseCSV
from .sources.parseOBO import parseOBO
from .sources.parseKeg import parseKeg
from .sources.parseSQLLite import parseSQLLite
from .sources.parseTSV import parseTSV
from .sources.parseTXT import parseTXT
from .sources.parseXML import parseXML 

from ..cache.loadCacheElseCreate import loadCacheElseCreate

def parser():
    print(f"{'=' * 10}PARSING{'=' * 10}")
    parsingStartTime = time()
    print("parsing and indexing ./data/OMIM/omim.txt")
    if not indexExist(OMIMtxtPattern):
        loadCacheElseCreate("./cache/omin.cache", lambda cp, p: parseTXT("./data/OMIM/omim.txt", OMIMtxt, cp, p), OMIMtxtPattern)
    else:
        print("skipping > index already exists")
    print("parsing and indexing ./data/OMIM/omim_onto.csv")
    if not indexExist(OMIM_ontoPattern):
        loadCacheElseCreate("./cache/omim_onto.cache", lambda cp, p: parseCSV("./data/OMIM/omim_onto.csv", OMIM_onto, cp, p), OMIM_ontoPattern)
    else:
        print("skipping > index already exists")
    print("parsing and indexing ./data/HPO/hpo_annotations.sqlite")
    if not indexExist(HPOAnnotationPattern):
        loadCacheElseCreate("./cache/hpo_annotations.cache", lambda cp, p: parseSQLLite("./data/HPO/hpo_annotations.sqlite", HPOAnnotation, cp, p, "SELECT * FROM 'phenotype_annotation';"), HPOAnnotationPattern)
    else:
        print("skipping > index already exists")
    print("parsing and indexing ./data/HPO/hpo.obo")
    if not indexExist(HPOOBOPattern):
        loadCacheElseCreate("./cache/hpo.cache", lambda cp, p: parseOBO("./data/HPO/hpo.obo", HPOOBO, cp, p), HPOOBOPattern)
    else:
        print("skipping > index already exists")
    print("parsing and indexing ./data/SIDER/meddra.tsv")
    if not indexExist(meddraPattern):    
        loadCacheElseCreate("./cache/meddra.cache", lambda cp, p : parseTSV("./data/SIDER/meddra.tsv", Meddra, cp, p), meddraPattern)
    else:
        print("skipping > index already exists")
    print("parsing and indexing ./data/SIDER/meddra_all_indications.tsv")
    if not indexExist(MeddraAllIndicationPattern):    
        loadCacheElseCreate("./cache/meddra_all_indications.cache", lambda cp, p: parseTSV("./data/SIDER/meddra_all_indications.tsv", MeddraAllIndication, cp, p), MeddraAllIndicationPattern)
    else:
        print("skipping > index already exists")
    print("parsing and indexing ./data/SIDER/meddra_all_se.tsv")
    if not indexExist(MeddraAllSePattern):    
        loadCacheElseCreate("./cache/meddra_all_se.cache", lambda cp, p: parseTSV("./data/SIDER/meddra_all_se.tsv", MeddraAllSe, cp, p), MeddraAllSePattern)
    else:
        print("skipping > index already exists")
    print("parsing and indexing ./data/SIDER/meddra_freq.tsv")
    if not indexExist(MeddraFreqPattern):    
        loadCacheElseCreate("./cache/meddra_freq.cache", lambda cp, p: parseTSV("./data/SIDER/meddra_freq.tsv", MeddraFreq, cp, p), MeddraFreqPattern)
    else:
        print("skipping > index already exists")
    print("parsing and indexing ./data/DrugBank/drugbank.xml")
    if not indexExist(drugBankPattern):
        loadCacheElseCreate("./cache/drugbank.cache", lambda cp, p: parseXML("./data/DrugBank/drugbank.xml", DrugBank, "drug", cp, p, minimalPropsCount=2), drugBankPattern)
    else:
        print("skipping > index already exists")
    print("parsing and indexing ./data/STITCH_ATC/br08303.keg")
    if not indexExist(KegPattern):
        loadCacheElseCreate("./cache/br08303.cache", lambda cp, p: parseKeg("./data/STITCH_ATC/br08303.keg", Keg, cp, p), KegPattern)
    else:
        print("skipping > index already exists")
    print("parsing and indexing ./data/STITCH_ATC/chemical.sources.v5.0.tsv")
    if not indexExist(ATCPattern):
        loadCacheElseCreate("./cache/chemical.sources.v5.0.cache", lambda cp, c: parseACT("./data/STITCH_ATC/chemical.sources.v5.0.tsv", ATCProps, cp, c), ATCPattern)
    else:
        print("skipping > index already exists")
    print(f"{'=' * 10}PARSING DONE IN {round(time() - parsingStartTime, 3)}s{'=' * 10}")