from time import time
from whoosh.qparser import QueryParser

from ..index.patterns import drugBankPattern, HPOAnnotationPattern, HPOOBOPattern, KegPattern, MeddraAllIndicationPattern, MeddraAllSePattern, MeddraFreqPattern, meddraPattern, OMIM_ontoPattern, OMIMtxtPattern
from ..index.index import getOrCreateIndex

def mapDrug(root, meddra_concept_name, target) : 
    drugBankS = getOrCreateIndex(drugBankPattern).searcher()   
    KegS = getOrCreateIndex(KegPattern).searcher()

    drugBankqp = QueryParser(target, schema=drugBankPattern["schema"])
    Kegqp = QueryParser("source", KegPattern["schema"])

    for DrugBankr in drugBankS.search(drugBankqp.parse(meddra_concept_name)):
        if "name" in DrugBankr:
            root[DrugBankr["name"]] = {"indication": [DrugBankr["indication"]], "side_effect": [], "chemicals": []}
            if "atc_code" in DrugBankr:
                for kegR in KegS.search(Kegqp.parse(DrugBankr["atc_code"])):
                    root[DrugBankr["name"]]["chemicals"].append(kegR["chemical"])
            root[DrugBankr["name"]]["side_effect"].append(meddra_concept_name)

def mapDisease(root, omimno) : 
    OMIM_ontoS = getOrCreateIndex(OMIM_ontoPattern).searcher()
    MeddraAllIndicationS = getOrCreateIndex(MeddraAllIndicationPattern).searcher()
    meddraS = getOrCreateIndex(meddraPattern).searcher()
    MeddraAllSeS = getOrCreateIndex(MeddraAllSePattern).searcher()

    OMIMOntoqp = QueryParser("classId", schema=OMIM_ontoPattern["schema"])
    MeddraAllIndicationqp = QueryParser("meddra_cui", schema=MeddraAllIndicationPattern["schema"])
    MeddraAllSeqp = QueryParser("meddra_cui", schema=MeddraAllSePattern["schema"])
    meddraqp = QueryParser("meddra_cui", schema=meddraPattern["schema"])

    for OMIMOntor in OMIM_ontoS.search(OMIMOntoqp.parse(omimno)):
        for MeddraAllIndicationr in MeddraAllIndicationS.search(MeddraAllIndicationqp.parse(" ".join(OMIMOntor["cui"].split("|")))):
            root["indication"].append({"type": MeddraAllIndicationr["meddra_concept_type"], "name": MeddraAllIndicationr["meddra_concept_name"]})
            for meddraR in meddraS.search(meddraqp.parse(MeddraAllIndicationr['meddra_cui'])):
                mapDrug(root["drug"], meddraR["meddra_concept_name"], "toxicity")
                mapDrug(root["drug"], meddraR["meddra_concept_name"], "indication")
            for meddraAllSeR in MeddraAllSeS.search(MeddraAllSeqp.parse(MeddraAllIndicationr['stich_compound_id'])):
                mapDrug(root["drug"], meddraAllSeR["side_effect"], "toxicity")
                mapDrug(root["drug"], meddraAllSeR["side_effect"], "indication")

def search(symptom):
    print(f"{'=' * 10}SEARCHING{'=' * 10}")
    searchingStartTime = time()
    
    HPOOBOS = getOrCreateIndex(HPOOBOPattern).searcher()
    HPOqp = QueryParser("name", schema=HPOOBOPattern["schema"])

    HPOAnnotationS = getOrCreateIndex(HPOAnnotationPattern).searcher()
    HPOAnnotationqp = QueryParser("sign_id", schema=HPOOBOPattern["schema"])
    OMIMtxtqp = QueryParser("TI", schema=OMIMtxtPattern["schema"])

    OMIMtxtS = getOrCreateIndex(OMIMtxtPattern).searcher()

    print(f"Searching with terms \"{symptom}\"")
    rSymptoms = {}
    for HPOr in HPOOBOS.search(HPOqp.parse(symptom)):
        rSymptoms[HPOr["name"]] = {"data": {}, "score": HPOr.score}
        HPOAnnotationRs = HPOAnnotationS.search(HPOAnnotationqp.parse(HPOr["id"]))
        for HPOAnnotationR in HPOAnnotationRs:
            rSymptoms[HPOr["name"]]["data"][HPOAnnotationR["disease_label"]] = {"drug": {}, "indication": []}
            for OMIMTXTr in OMIMtxtS.search(OMIMtxtqp.parse(HPOAnnotationR["disease_label"])):
                mapDisease(rSymptoms[HPOr["name"]]["data"][HPOAnnotationR["disease_label"]], OMIMTXTr["NO"])

    OMIMCSqp = QueryParser("CS", schema=OMIMtxtPattern["schema"])
    for OMIMCSr in OMIMtxtS.search(OMIMCSqp.parse(symptom)):
        if OMIMCSr["CS"] not in rSymptoms:
            rSymptoms[OMIMCSr["CS"]] = {"data": {}, "score": OMIMCSr.score}

        if OMIMCSr["TI"] not in rSymptoms[OMIMCSr["CS"]]["data"]:
            rSymptoms[OMIMCSr["CS"]]["data"][OMIMCSr["TI"]] = {"drug": {}, "indication": []}

        mapDisease(rSymptoms[OMIMCSr["CS"]]["data"][OMIMCSr["TI"]], OMIMCSr["NO"])
    
    print(f"{'=' * 10}SEARCHING DONE IN {round(time() - searchingStartTime, 3)}s{'=' * 10}")
    return rSymptoms
    

