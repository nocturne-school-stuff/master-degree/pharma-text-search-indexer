from json import loads
from os.path import exists

from ..index.index import getOrCreateIndex

def loadCacheElseCreate(cachePath, toExecuteIfNotExists, pattern) :     
    if exists(cachePath):
        idx = getOrCreateIndex(pattern)
        w = idx.writer()
        with open(cachePath, mode="r") as cacheFile:
            try:
                while(True):
                    w.add_document(**loads(cacheFile.readline()))
            except Exception as e:
                print(e)
        w.commit()
    else:
        toExecuteIfNotExists(cachePath, pattern)