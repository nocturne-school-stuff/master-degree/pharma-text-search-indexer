from whoosh.fields import Schema, TEXT, ID, KEYWORD, NUMERIC

globalPattern = {
    "name": "globalPattern",
    "path": "./index",
    "schema": Schema(drug=ID(stored=True), disease=ID(stored=True), symptomes=KEYWORD(stored=True), indication=KEYWORD(stored=True), side_effects=KEYWORD(stored=True))
}

drugBankPattern = {
    "name": "drugBank",
    "path": "./index/drugBank",
    "schema": Schema(name=ID(stored=True), indication=TEXT(stored=True), toxicity=TEXT(stored=True), atc_code=ID(stored=True))
}

HPOOBOPattern = {
    "name": "HPOOBO",
    "path": "./index/HPO",
    "schema": Schema(id=ID(stored=True), name=TEXT(stored=True), is_a=KEYWORD(stored=True), alt_id=KEYWORD(stored=True))
}

OMIMtxtPattern = {
    "name": "OMIMtxt",
    "path": "./index/OMIMtxt",
    "schema": Schema(NO=ID(stored=True), CS=TEXT(stored=True), TI=TEXT(stored=True))
}

OMIM_ontoPattern = {
    "name": "OMIMOnto",
    "path": "./index/OMIMOnto",
    "schema": Schema(classId=ID(stored=True), cui=ID(stored=True))
}

HPOAnnotationPattern = {
    "name": "HPOAnnotation",
    "path": "./index/HPOAnnotation",
    "schema": Schema(disease_label=ID(stored=True), sign_id=ID(stored=True))
}

meddraPattern = {
    "name": "meddra",
    "path": "./index/meddra",
    "schema": Schema(meddra_id=ID(stored=True), meddra_cui=ID(stored=True), meddra_concept_type=KEYWORD(stored=True), meddra_concept_name=KEYWORD(stored=True))
}

MeddraAllIndicationPattern = {
    "name": "MeddraAllIndication",
    "path": "./index/MeddraAllIndication",
    "schema": Schema(stich_compound_id=ID(stored=True), meddra_cui=ID(stored=True), concept_name=KEYWORD(stored=True), method_of_detection=TEXT(stored=True), meddra_concept_type=KEYWORD(stored=True), meddra_concept_name=KEYWORD(stored=True))
}

MeddraAllSePattern = {
    "name": "MeddraAllSe",
    "path": "./index/MeddraAllSe",
    "schema": Schema(meddra_cui=ID(stored=True), cui=ID(stored=True), stich_compound_id1=ID(stored=True), stich_compound_id2=ID(stored=True), meddra_concept_type=KEYWORD(stored=True), side_effect=TEXT(stored=True))
}

MeddraFreqPattern = {
    "name": "MeddraFreq",
    "path": "./index/MeddraFreq",
    "schema": Schema(cui=ID(stored=True), stich_compound_id1=ID(stored=True), stich_compound_id2=ID(stored=True), frequency_description=KEYWORD(stored=True), freq_lower_bound=NUMERIC(stored=True), freq_upper_bound=NUMERIC(stored=True), placebo=TEXT(stored=True), side_effect=TEXT(stored=True), meddra_concept_type=KEYWORD(stored=True), meddra_concept_id=KEYWORD(stored=True))
}

KegPattern = {
    "name": "Keg",
    "path": "./index/Keg",
    "schema": Schema(source=ID(stored=True), chemical=KEYWORD(stored=True))
}

ATCPattern = {
    "name": "ATC",
    "path": "./index/ATC",
    "schema": Schema(cid=ID(stored=True), source=KEYWORD(stored=True), atc_code=ID(stored=True))
}