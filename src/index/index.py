from os import makedirs
from os.path import exists
from whoosh.index import create_in, exists_in, open_dir

def getOrCreateIndex(pattern):
    if not exists(pattern["path"]):
        makedirs(pattern["path"])
    if exists_in(pattern["path"], pattern["name"]):
        return open_dir(pattern["path"], pattern["name"])
    return create_in(pattern["path"], pattern["schema"], pattern["name"])

def indexExist(pattern):
    return exists_in(pattern["path"], pattern["name"])
   
